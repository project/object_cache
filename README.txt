
-- SUMMARY --

For a full description of the module, visit the project page:
  http://drupal.org/project/object_cache
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/object_cache

-- MAINTAINERS --

swentel - http://drupal.org/user/107403
